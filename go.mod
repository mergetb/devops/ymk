module gitlab.com/mergetb/devops/ymk

go 1.14

require (
	github.com/fatih/color v1.7.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.1.1
	gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776
)
