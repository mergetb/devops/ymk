Name:           ymk
Version:        %{_version}
Release:        %{_release}%{?dist}
Summary:        Golang-based build tool to replace Make files with yaml files

License:        ASL 2.0
URL:            https://gitlab.com/mergetb/devops/%{name}
Source0:        https://gitlab.com/mergetb/devops/%{name}/-/releases/%{version}/%{name}-%{version}.tar.gz
BuildRequires:  golang

# this undefine fixes expectation of debugger entities
%global debug_package %{nil}

%description
The Yaml Make tool is intended to provide a simpler, golang-based interface for building software.

# -s == file is not zero size, ! -s == file should be zero size
%prep
mkdir -vp %{_sourcedir}
pushd %{_sourcedir}
[[ -s %{name}-%{version}.tar.gz ]] || curl -sO https://gitlab.com/mergetb/devops/%{name}/-/archive/%{version}/%{name}-%{version}.tar.gz
popd
%autosetup

%build
# output the current directory & build if binary does not yet exist
# the ymk binary/artifact may already exist in the gitlab CI pipeline
echo "PWD and directory listing" && pwd && ls -lh
[[ -e ymk ]] || CGO_ENABLED=0 go build -a -v .


%install
rm -rf $RPM_BUILD_ROOT
install -D -m 0755 example/hello/build.yml %{buildroot}/%{_datadir}/%{name}/examples/hello/build.yml
install -D -m 0755 %{name} %{buildroot}/%{_bindir}/%{name}


%files
%{_bindir}/%{name}
%{_datadir}/%{name}/examples/hello/build.yml
%license LICENSE
%doc README.md



%changelog
* Sat May 28 2022 jdbarnes
- initial trial of ymk rpm spec file, no code changes.
