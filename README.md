# **ymk**
**ymk** is a custom build tool used for building various MergeTB
projects, such as the
[Portal Appliance](https://gitlab.com/mergetb/portal/appliance/).

# install

## download static binary

To install a pre-built binary to `/usr/bin/`, run:
```shell
wget https://gitlab.com/mergetb/devops/ymk/-/jobs/artifacts/master/raw/ymk?job=make
sudo chmod +x ymk
sudo cp ymk /usr/bin/ymk
```
## build static binary
### prerequisites
- Go (golang)
  * (Fedora|Debian): `sudo (dnf|apt) install golang`
- Make (GNU)
  * `(apt|dnf|yum|zypper) install make`

To compile and install, run:
```shell
git clone https://gitlab.com/mergetb/devops/ymk
cd ymk
make
sudo make install
```
## Fedora RPM

To install a ci produced rpm on Fedora, install the mergetb repo, then install the ymk rpm:
```shell
curl -s https://pkg.mergetb.net/rpms/fedora/mergetb.repo | sudo tee /etc/yum.repos.d/mergetb.repo
```

```
[mergetb]
name=MergeTB_Fedora${releasever}.$basearch
baseurl=https://pkg.mergetb.net/rpms/fedora/$releasever/$basearch
enabled=1
countme=1
metadata_expire=7d
repo_gpgcheck=1
type=rpm
gpgcheck=1
gpgkey=http://pkg.mergetb.net/gpg.2022
skip_if_unavailable=True
```
then, install

```
sudo dnf install ymk
```

# usage

```
ymk <build.yml> [flags]

Flags:
  -h, --help       help for ymk
  -j, --jobs int   limit simultaneous jobs
```

# examples

After compiling and installing `ymk`, run the following example:
```shell
$ cd ymk/example/hello
$ ymk build.xml
✅ world: echo world > world.txt
✅ post: echo POST
  POST
✅ pre: echo PRE
  PRE
✅ hello: echo hello > hello.txt
✅ hello_world: cat hello.txt
  hello
✅ hello_world: cat world.txt
  world
✅ hello_world: echo tacos:/home/calvin/.local/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin
  tacos:/home/calvin/.local/bin:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin
```

# License

[Apache Software License version 2.0](https://www.apache.org/licenses/LICENSE-2.0)

[alternative link](https://opensource.org/licenses/Apache-2.0)
