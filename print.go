package main

import (
	"fmt"
	"os"

	"github.com/fatih/color"
)

var red = color.New(color.FgRed).SprintFunc()
var cyan = color.New(color.FgCyan).SprintFunc()

func printHeader(name string) {

	color.Cyan(name)

}

func (spec *Spec) failStep(step, err, out string) {

	fmt.Printf("%s %s: %s\n%s\n%s", "❌", cyan(spec.name), step, err, out)
	os.Exit(1)

}

func (spec *Spec) printStep(cmd string, out string) {

	if len(out) == 0 {
		fmt.Printf("✅ %s: %s\n", cyan(spec.name), cmd)
	} else {
		fmt.Printf("✅ %s: %s\n  %s", cyan(spec.name), cmd, out)
	}

}

func (spec *Spec) printDryrun(cmd string) {

	fmt.Printf("✓ %s: %s\n", cyan(spec.name), cmd)

}

func fatal(msg interface{}) {

	fmt.Printf("%s %s\n", red("error:"), msg)
	os.Exit(1)

}

func fatalf(msg string, args ...interface{}) {

	fatal(fmt.Sprintf(msg, args...))

}
