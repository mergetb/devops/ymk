package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"sync"
	"text/template"
	"time"

	"github.com/spf13/cobra"
	"gopkg.in/yaml.v3"
)

type Source struct {
	Var   map[string]string
	Env   map[string]string
	Specs `yaml:",inline"`
}

type Specs map[string]*Spec
type Spec struct {
	In      []string
	Out     []string
	Build   []string
	Var     map[string]string
	Env     map[string]string
	Workdir string
	Dryrun  bool

	in, out []*Spec
	fin     []string
	rank    uint
	done    bool
	name    string
	mtx     sync.Mutex
}

var src *Source
var jobs int

func main() {

	root := &cobra.Command{
		Use:   "ymk <build.yml>",
		Short: "ymake",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			run(args[0])
		},
	}

	root.PersistentFlags().IntVarP(&jobs, "jobs", "j", 0, "limit simultaneous jobs")

	root.Execute()

}

func run(srcfile string) {

	buf, err := ioutil.ReadFile(srcfile)
	if err != nil {
		fatal(err)
	}

	src = &Source{
		Var:   make(map[string]string),
		Env:   make(map[string]string),
		Specs: make(Specs),
	}

	err = yaml.Unmarshal(buf, src)
	if err != nil {
		fatal(err)
	}

	src.applyLocalEnv()

	processSource()

}

func (src *Source) applyLocalEnv() {

	for k := range src.Env {
		v := os.Getenv(k)
		if v != "" {
			src.Env[k] = v
		}
	}

}

func (src *Source) resolveIn(
	ref string, outmap map[string]*Spec) (*Spec, error) {

	// try to resolve as target
	target := src.Specs[ref]
	if target != nil {
		return target, nil
	}

	target = outmap[ref]
	if target != nil {
		return target, nil
	}

	_, err := os.Stat(ref)
	if err == nil {
		return nil, nil
	}

	return nil, fmt.Errorf("%s referenced but not defined", ref)

}

func processSource() {

	outmap := make(map[string]*Spec)
	for _, spec := range src.Specs {
		if spec.Var == nil {
			spec.Var = make(map[string]string)
		}
		if spec.Env == nil {
			spec.Env = make(map[string]string)
		}
		for i, out := range spec.Out {
			spec.Out[i] = spec.resolveRef(out, false)
		}
		for _, out := range spec.Out {
			outmap[out] = spec
		}
		spec.expandEnv()
	}

	for name, spec := range src.Specs {
		spec.name = name
		for _, in := range spec.In {
			target, err := src.resolveIn(in, outmap)
			if err != nil {
				fatal(err)
			}
			if target != nil {
				spec.in = append(spec.in, target)
				target.out = append(target.out, spec)
			} else {
				spec.fin = append(spec.fin, in)
			}
		}
	}

	var tasks []*Spec
	for _, spec := range src.Specs {
		rank(spec, 0)
		tasks = append(tasks, spec)
	}

	sort.Slice(tasks, func(i, j int) bool {
		return tasks[i].rank < tasks[j].rank
	})

	var wg sync.WaitGroup

	var guard chan struct{}
	if jobs >= 1 {
		guard = make(chan struct{}, jobs)
	}

	// root nodes have rank 0, recursively execute task tree from each root node
	for _, t := range tasks {
		if t.rank > 0 {
			break
		}

		if jobs >= 1 {
			guard <- struct{}{}
		}

		wg.Add(1)
		go func(t *Spec, wg *sync.WaitGroup) {
			execute(t)
			wg.Done()
			if jobs >= 1 {
				<-guard
			}
		}(t, &wg)

	}
	wg.Wait()

}

func rank(spec *Spec, depth uint) {
	if depth > spec.rank {
		spec.rank = depth
	}
	for _, in := range spec.in {
		rank(in, depth+1)
	}
}

func execute(spec *Spec) time.Time {

	spec.mtx.Lock()
	defer spec.mtx.Unlock()

	stale := false
	var inTime time.Time
	exTime := time.Now()

	if len(spec.in) > 0 {

		ch := make(chan time.Time)

		for _, in := range spec.in {
			go func(in *Spec) {
				ch <- execute(in)
			}(in)
		}

		for i := 0; i < len(spec.in); i++ {
			t := <-ch
			if t.After(inTime) {
				inTime = t
			}
		}

	}

	if len(spec.fin) > 0 {
		for _, in := range spec.fin {
			stat, err := os.Stat(in)
			if err != nil {
				fatal(err)
			}
			if stat.ModTime().After(inTime) {
				inTime = stat.ModTime()
			}

		}
	}

	if len(spec.Out) > 0 {
		for _, x := range spec.Out {
			stat, err := os.Stat(x)
			if err != nil {
				stale = true
				break
			}
			if inTime.After(stat.ModTime()) {
				stale = true
				break
			}
		}
	} else {
		if inTime.After(exTime) {
			stale = true
		}
	}

	if len(spec.Out) == 0 && len(spec.fin) == 0 && len(spec.in) == 0 {
		stale = true
	}

	outTime := inTime
	if stale {
		for _, x := range spec.Build {
			spec.runStep(x)
		}
		outTime = time.Now()
	}

	return outTime
}

func (spec *Spec) runStep(step string) {

	_step := spec.interpret(step)
	//spec.printStep(_step, "")

	if spec.Workdir != "" {
		spec.Workdir = spec.resolveRef(spec.Workdir, false)
		_, err := os.Stat(spec.Workdir)
		if err != nil {
			if os.IsNotExist(err) {
				err = os.MkdirAll(spec.Workdir, 0755)
				if err != nil {
					spec.failStep(
						fmt.Sprintf("mkdir -p %s", spec.Workdir),
						err.Error(),
						"",
					)
				}
			} else {
				spec.failStep(
					fmt.Sprintf("stat dir %s", spec.Workdir),
					err.Error(),
					"",
				)
			}
		}
	}

	c := append([]string{"-c"}, _step)
	cmd := exec.Command("bash", c...)
	cmd.Env = os.Environ()
	cmd.Dir = spec.Workdir
	for k, v := range src.Env {
		cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%s", k, v))
	}
	for k, v := range spec.Env {
		cmd.Env = append(cmd.Env, fmt.Sprintf("%s=%s", k, v))
	}
	if spec.Dryrun {
		spec.printDryrun(_step)
		return
	}

	out, err := cmd.CombinedOutput()
	if err != nil {
		spec.failStep(_step, err.Error(), string(out))
	}

	spec.printStep(_step, string(out))

}

var refx = regexp.MustCompile(`\$([a-zA-Z_][a-zA-Z0-9_\[\]:]*)`)

func (spec *Spec) resolveRef(part string, fromEnv bool) string {

	mss := refx.FindAllStringSubmatch(part, -1)
	for _, ms := range mss {
		if len(ms) > 1 {
			for _, k := range ms[1:] {
				t, err := spec.template("{{." + strings.Title(k) + "}}")
				if err == nil {
					part = strings.Replace(part, "$"+k, t, 1)
				}
				v, ok := spec.Var[k]
				if ok {
					part = strings.Replace(part, "$"+k, v, 1)
				} else {
					v, ok := src.Var[k]
					if ok {
						part = strings.Replace(part, "$"+k, v, 1)
					}
				}
				if fromEnv {
					e := os.Getenv(k)
					if len(e) > 0 {
						part = strings.Replace(part, "$"+k, e, 1)
					}
				} else {
					e, ok := spec.Env[k]
					if ok {
						part = strings.Replace(part, "$"+k, e, 1)
					} else {
						e, ok := src.Env[k]
						if ok {
							part = strings.Replace(part, "$"+k, e, 1)
						}
					}
				}
				// at this point assume it's an env variable to be expanded by
				// bash
			}
		}
	}

	return part

}

func (spec *Spec) interpret(step string) string {

	parts := strings.Fields(step)

	for i, part := range parts {
		parts[i] = spec.resolveRef(part, false)
	}

	return strings.Join(parts, " ")

}

func (spec *Spec) expandEnv() {
	for k, v := range spec.Env {
		if strings.Contains(v, "$") {
			spec.Env[k] = spec.resolveRef(v, true)
		}
	}
	for k, v := range src.Env {
		if strings.Contains(v, "$") {
			spec.Env[k] = spec.resolveRef(v, true)
		}
	}

}

type SpecTemplate struct {
	out []string
}

func (spec *Spec) template(part string) (string, error) {

	part = handleArrayIndex(part)
	part = handleSlice(part)

	tmpl, err := template.New("spec").Parse(part)
	if err != nil {
		fatal(err)
	}

	var out bytes.Buffer
	err = tmpl.Execute(&out, spec)
	if err != nil {
		return "", err
	}

	result := out.String()

	result = strings.ReplaceAll(result, "[", "")
	result = strings.ReplaceAll(result, "]", "")

	return result, nil

}

func handleArrayIndex(part string) string {

	re := regexp.MustCompile(`{{(.*)\[([0-9]+)\]}}`)

	out := re.ReplaceAll(
		[]byte(part),
		[]byte("{{ index ${1} ${2} }}"),
	)
	return string(out)

}

func handleSlice(part string) string {

	re := regexp.MustCompile(`{{(.*)\[([0-9]*)\:\]}}`)

	out := re.ReplaceAll(
		[]byte(part),
		[]byte("{{ slice ${1} ${2} }}"),
	)

	part = string(out)

	re = regexp.MustCompile(`{{(.*)\[([0-9]*)\:([0-9]*)\]}}`)

	matches := re.FindStringSubmatch(part)
	if len(matches) != 4 {
		return part
	}

	begin, err := strconv.Atoi(matches[2])
	if err != nil {
		return part
	}

	end, err := strconv.Atoi(matches[3])
	if err != nil {
		return part
	}

	return fmt.Sprintf("{{ slice %s %d %d }}", matches[1], begin, end+1)

	/*
		out = re.ReplaceAll(
			[]byte(part),
			[]byte("{{ slice ${1} ${2} ${3}+1 }}"),
		)

		return string(out)
	*/

}

/*
func execute(src Source) {

	for name, spec := range src {

		ready := true
		for _, in := range spec.in {
			if !in.done {
				ready = false
				break
			}
		}
		if ready {
			//go func(name string, spec *Spec) {
			log.Info(name)
			//}(name, spec)
		}

	}

}
*/
