PREFIX  ?= /usr/local
DESTDIR ?=
BINDIR  ?= $(PREFIX)/bin

all: ymk

ymk: $(wildcard *.go)
	go build -v -o "$@"

install: ymk
	install -d $(DESTDIR)$(BINDIR)
	install -m 755 ymk $(DESTDIR)$(BINDIR)

clean:
	go clean
	$(RM) ymk

.PHONY: all clean install
